<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductCategory;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with('categoryRef')->get();
        return view('products.index', compact('products')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = ProductCategory::all();
        return view('products.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $extension = $request->image->extension();
        $namafile = str_replace("-","",date("Y-m-d")).substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUFWXYZ'),1,3);
        $request->image->move(public_path('/uploads/products'), $namafile.'.'.$extension);

        $products = new Product;
        $products->product_category_id = $request->category;
        $products->name = $request->name;
        $products->image = $namafile.'.'.$extension;
        $products->desc = $request->desc;
        $products->price = $request->price;
        $products->save();
        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $products = Product::with('pinjam')->findOrFail($id);;
        return view('products.history', compact('products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $category = ProductCategory::all();
        return view('products.edit', compact('category', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $old = public_path()."/uploads/products/".$product->image;
        if(file_exists($old))
        {
            unlink($old);
        }
        $extension = $request->image->extension();
        $namafile = str_replace("-","",date("Y-m-d")).substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUFWXYZ'),1,3);
        $request->image->move(public_path('/uploads/products'), $namafile.'.'.$extension);

        $product->product_category_id = $request->category;
        $product->name = $request->name;
        $product->image = $namafile.'.'.$extension;
        $product->desc = $request->desc;
        $product->price = $request->price;
        $product->update();
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('product.index');
    }
}
