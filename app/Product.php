<?php

namespace App;

use SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function categoryRef()
    {
        return $this->belongsTo(ProductCategory::class, 'product_category_id');
    } 
    
    public function pinjam()
    {
        return $this->hasMany(Transaksi::class, 'product_id');
    } 
}
