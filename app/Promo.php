<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
    public function categoryRef()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
