@extends('layouts.template')

@section('content')
<div class="container-fluid">
    <div class="block-header">
        <h2>DASHBOARD</h2>
    </div>
    <!-- Widgets -->
    <div class="row clearfix">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-pink hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">playlist_add_check</i>
                </div>
                <div class="content">
                    <div class="text">NEW TASKS</div>
                    <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">125</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-cyan hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">help</i>
                </div>
                <div class="content">
                    <div class="text">NEW TICKETS</div>
                    <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20">257</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-light-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">forum</i>
                </div>
                <div class="content">
                    <div class="text">NEW COMMENTS</div>
                    <div class="number count-to" data-from="0" data-to="243" data-speed="1000" data-fresh-interval="20">243</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-orange hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">person_add</i>
                </div>
                <div class="content">
                    <div class="text">NEW VISITORS</div>
                    <div class="number count-to" data-from="0" data-to="1225" data-speed="1000" data-fresh-interval="20">1225</div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- #END# Widgets -->
    <div class="row clearfix">
        <!-- Task Info -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>TASK INFOS</h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-hover dashboard-task-infos">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Task</th>
                                    <th>Status</th>
                                    <th>Manager</th>
                                    <th>Progress</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Task A</td>
                                    <td><span class="label bg-green">Doing</span></td>
                                    <td>John Doe</td>
                                    <td>
                                        <div class="progress">
                                            <div class="progress-bar bg-green" role="progressbar" aria-valuenow="62" aria-valuemin="0" aria-valuemax="100" style="width: 62%"></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Task B</td>
                                    <td><span class="label bg-blue">To Do</span></td>
                                    <td>John Doe</td>
                                    <td>
                                        <div class="progress">
                                            <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Task C</td>
                                    <td><span class="label bg-light-blue">On Hold</span></td>
                                    <td>John Doe</td>
                                    <td>
                                        <div class="progress">
                                            <div class="progress-bar bg-light-blue" role="progressbar" aria-valuenow="72" aria-valuemin="0" aria-valuemax="100" style="width: 72%"></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Task D</td>
                                    <td><span class="label bg-orange">Wait Approvel</span></td>
                                    <td>John Doe</td>
                                    <td>
                                        <div class="progress">
                                            <div class="progress-bar bg-orange" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%"></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Task E</td>
                                    <td>
                                        <span class="label bg-red">Suspended</span>
                                    </td>
                                    <td>John Doe</td>
                                    <td>
                                        <div class="progress">
                                            <div class="progress-bar bg-red" role="progressbar" aria-valuenow="87" aria-valuemin="0" aria-valuemax="100" style="width: 87%"></div>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Task Info -->
            <!-- Browser Usage -->
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="card">
                    <div class="header">
                        <h2>BROWSER USAGE</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div id="donut_chart" class="dashboard-donut-chart"><svg height="265" version="1.1" width="280" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative; top: -0.199951px;"><desc>Created with Raphaël 2.2.0</desc><defs></defs><path style="opacity: 1;" fill="none" stroke="#e91e63" d="M140,214.16666666666669A81.66666666666667,81.66666666666667,0,0,0,202.36598314293147,79.77497187236375" stroke-width="2" opacity="1"></path><path style="" fill="#e91e63" stroke="#ffffff" d="M140,217.16666666666669A84.66666666666667,84.66666666666667,0,0,0,204.65697844205954,77.83813410440976L233.5489747143972,53.41245780854564A122.5,122.5,0,0,1,140,255Z" stroke-width="3"></path><path style="opacity: 0;" fill="none" stroke="#00bcd4" d="M202.36598314293147,79.77497187236375A81.66666666666667,81.66666666666667,0,0,0,71.93689880580898,87.36842900724861" stroke-width="2" opacity="0"></path><path style="" fill="#00bcd4" stroke="#ffffff" d="M204.65697844205954,77.83813410440976A84.66666666666667,84.66666666666667,0,0,0,69.4366216190836,85.71053456261693L42.07247685325579,67.56580091859239A117.5,117.5,0,0,1,229.73064921585035,56.64052075513561Z" stroke-width="3"></path><path style="opacity: 0;" fill="none" stroke="#ff9800" d="M71.93689880580898,87.36842900724861A81.66666666666667,81.66666666666667,0,0,0,70.44216513257355,175.29196248129173" stroke-width="2" opacity="0"></path><path style="" fill="#ff9800" stroke="#ffffff" d="M69.4366216190836,85.71053456261693A84.66666666666667,84.66666666666667,0,0,0,67.8869793619334,176.86391212346163L39.92189064992726,194.06802765165443A117.5,117.5,0,0,1,42.07247685325579,67.56580091859239Z" stroke-width="3"></path><path style="opacity: 0;" fill="none" stroke="#009688" d="M70.44216513257355,175.29196248129173A81.66666666666667,81.66666666666667,0,0,0,120.52090387726372,211.80957860615354" stroke-width="2" opacity="0"></path><path style="" fill="#009688" stroke="#ffffff" d="M67.8869793619334,176.86391212346163A84.66666666666667,84.66666666666667,0,0,0,119.80534524418361,214.72299169780814L111.97395353769576,246.60867942313925A117.5,117.5,0,0,1,39.92189064992726,194.06802765165443Z" stroke-width="3"></path><path style="opacity: 0;" fill="none" stroke="#607d8b" d="M120.52090387726372,211.80957860615354A81.66666666666667,81.66666666666667,0,0,0,139.9743436604178,214.16666263657822" stroke-width="2" opacity="0"></path><path style="" fill="#607d8b" stroke="#ffffff" d="M119.80534524418361,214.72299169780814A84.66666666666667,84.66666666666667,0,0,0,139.97340118263722,217.1666624885342L139.96308628692765,249.99999420160748A117.5,117.5,0,0,1,111.97395353769576,246.60867942313925Z" stroke-width="3"></path><text style="text-anchor: middle; font-family: &quot;Arial&quot;; font-size: 15px; font-weight: 800;" x="140" y="122.5" text-anchor="middle" font-family="&quot;Arial&quot;" font-size="15px" stroke="none" fill="#000000" font-weight="800" transform="matrix(2.2685,0,0,2.2685,-177.5926,-166.8102)" stroke-width="0.4408163265306122"><tspan dy="5">Chrome</tspan></text><text style="text-anchor: middle; font-family: &quot;Arial&quot;; font-size: 14px;" x="140" y="142.5" text-anchor="middle" font-family="&quot;Arial&quot;" font-size="14px" stroke="none" fill="#000000" transform="matrix(1.7014,0,0,1.7014,-98.1944,-94.3368)" stroke-width="0.5877551020408163"><tspan dy="5">37%</tspan></text></svg></div>
                    </div>
                </div>
            </div>
            <!-- #END# Browser Usage -->
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>BAR CHART</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div id="bar_chart" class="graph" style="position: relative;">
                        <svg height="342" version="1.1" width="455" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative; top: -0.399994px;"><desc>Created with Raphaël 2.2.0</desc><defs></defs><text style="text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" x="20.133333206176758" y="308" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" font-weight="normal"><tspan dy="4">0</tspan></text><path style="" fill="none" stroke="#aaaaaa" d="M32.63333320617676,308H430" stroke-width="0.5"></path><text style="text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" x="20.133333206176758" y="237.25" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" font-weight="normal"><tspan dy="4">1</tspan></text><path style="" fill="none" stroke="#aaaaaa" d="M32.63333320617676,237.25H430" stroke-width="0.5"></path><text style="text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" x="20.133333206176758" y="166.5" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" font-weight="normal"><tspan dy="4">2</tspan></text><path style="" fill="none" stroke="#aaaaaa" d="M32.63333320617676,166.5H430" stroke-width="0.5"></path><text style="text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" x="20.133333206176758" y="95.75" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" font-weight="normal"><tspan dy="4">3</tspan></text><path style="" fill="none" stroke="#aaaaaa" d="M32.63333320617676,95.75H430" stroke-width="0.5"></path><text style="text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" x="20.133333206176758" y="25" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" font-weight="normal"><tspan dy="4">4</tspan></text><path style="" fill="none" stroke="#aaaaaa" d="M32.63333320617676,25H430" stroke-width="0.5"></path><text style="text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" x="380.3291666507721" y="320.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" font-weight="normal" transform="matrix(1,0,0,1,0,7.5)"><tspan dy="4">2011 Q4</tspan></text><text style="text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" x="181.64583325386047" y="320.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" font-weight="normal" transform="matrix(1,0,0,1,0,7.5)"><tspan dy="4">2011 Q2</tspan></text><rect x="45.051041543483734" y="95.75" width="22.835416674613953" height="212.25" rx="0" ry="0" fill="#e91e63" stroke="none" style="fill-opacity: 1;" fill-opacity="1"></rect><rect x="70.88645821809769" y="166.5" width="22.835416674613953" height="141.5" rx="0" ry="0" fill="#00bcd4" stroke="none" style="fill-opacity: 1;" fill-opacity="1"></rect><rect x="96.72187489271164" y="95.75" width="22.835416674613953" height="212.25" rx="0" ry="0" fill="#009688" stroke="none" style="fill-opacity: 1;" fill-opacity="1"></rect><rect x="144.39270824193954" y="166.5" width="22.835416674613953" height="141.5" rx="0" ry="0" fill="#e91e63" stroke="none" style="fill-opacity: 1;" fill-opacity="1"></rect><rect x="196.06354159116745" y="237.25" width="22.835416674613953" height="70.75" rx="0" ry="0" fill="#009688" stroke="none" style="fill-opacity: 1;" fill-opacity="1"></rect><rect x="243.73437494039536" y="308" width="22.835416674613953" height="0" rx="0" ry="0" fill="#e91e63" stroke="none" style="fill-opacity: 1;" fill-opacity="1"></rect><rect x="269.5697916150093" y="166.5" width="22.835416674613953" height="141.5" rx="0" ry="0" fill="#00bcd4" stroke="none" style="fill-opacity: 1;" fill-opacity="1"></rect><rect x="295.40520828962326" y="25" width="22.835416674613953" height="283" rx="0" ry="0" fill="#009688" stroke="none" style="fill-opacity: 1;" fill-opacity="1"></rect><rect x="343.07604163885117" y="166.5" width="22.835416674613953" height="141.5" rx="0" ry="0" fill="#e91e63" stroke="none" style="fill-opacity: 1;" fill-opacity="1"></rect><rect x="368.9114583134651" y="25" width="22.835416674613953" height="283" rx="0" ry="0" fill="#00bcd4" stroke="none" style="fill-opacity: 1;" fill-opacity="1"></rect><rect x="394.74687498807907" y="95.75" width="22.835416674613953" height="212.25" rx="0" ry="0" fill="#009688" stroke="none" style="fill-opacity: 1;" fill-opacity="1"></rect></svg><div class="morris-hover morris-default-style" style="left: 46.3042px; top: 127px;"><div class="morris-hover-row-label">2011 Q1</div><div class="morris-hover-point" style="color: rgb(233, 30, 99)">
                            Y:
                            3
                            </div><div class="morris-hover-point" style="color: rgb(0, 188, 212)">
                            Z:
                            2
                            </div><div class="morris-hover-point" style="color: rgb(0, 150, 136)">
                            A:
                            3
                            </div></div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>      
</div>
@endsection
