@extends('layouts.template')

@section('content')
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h2>Tambah Data Anggota</h2>
        </div>
        <form action="{{ route('member.store') }}" method="post">
            @csrf
        <div class="body">
            <form id="form_validation" method="POST" novalidate="novalidate">
            <div class="form-group form-float">
                <label">Produk Kategori</label>
                <select class="form-control show-tick" name="category" id="category">
                    <option>chose...</option>
                        @foreach ($category as $item)
                        <option value="{{ $item->id }}"> {{ $item->name }}</option>
                        @endforeach
                </select>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="full_name" required="" aria-required="true">
                        <label class="form-label">Nama Lengkap</label>
                    </div>
                </div>
                <div class="form-group form-float">
                <div>
                        <label">Tanggal Lahir</label>
                        <input type="date" class="form-control" name="dob" required="" aria-required="true">
                        <div class="form-control show-tick"></div>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="address" required="" aria-required="true">
                        <label class="form-label">Alamat</label>
                    </div>
                </div>
                <div class="form-group">
                <label">Gender</label>
                <div class="show-tick"></div>
                    <input type="radio" name="gender" id="gender1" value="pria" class="with-gap">
                    <label for="gender1">Pria</label>

                    <input type="radio" name="gender" id="gender2" value="wanita" class="with-gap">
                    <label for="gender2" class="m-l-20">Wanita</label>
                </div>
                <button class="btn btn-danger waves-effect" type="submit">SIMPAN</button>
            </form>
        </div>
    </div>
</div>
@endsection