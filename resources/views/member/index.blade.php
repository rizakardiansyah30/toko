@extends('layouts.template')

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Data Barang
                </h2>
                <ul class="header-dropdown m-r--5">
                <li>
                    <a href="{{ route('member.create') }}" >
                        <button type="button" class="btn btn-danger btn-sm">Tambah</button>
                    </a>
                 </li>
                </ul>
            </div>
            <div class="body table-responsive">
                <table class="table table-condensed">
                    <thead>
                        <tr class="bg-deep-orange">
                            <th>NO</th>
                            <th>Kategori</th>
                            <th>NAMA</th>
                            <th>TANGGAL LAHIR</th>
                            <th>ALAMAT</th>
                            <th>GENDER</th>
                            <th>KODE MEMBER</th>
                            <th>PILIHAN</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($members as $i => $item)
                            <tr class="bg-danger">
                                <th>{{ $i+1 }}</th>
                                <td>{{ $item->categoryRef->name }}</td>
                                <td>{{ $item->full_name }}</td>
                                <td>{{ $item->dob }}</td>
                                <td>{{ $item->address }}</td>
                                <td>{{ $item->gender }}</td>
                                <td>{{ $item->barcode }}</td>
                                <td>
                                    <form action="{{ route ('member.destroy',$item->id) }} " method="post">
                                        <a href="{{ route ('member.edit', $item->id) }}">
                                            <button type="button" class="btn btn-success">Ubah</button>
                                        </a>
                                        @csrf 
                                        @method('delete')
                                            <button type="submit" class="btn btn-danger" 
                                            onclick="return confirm('Yakin Dihapus?' )">Hapus</button>
                                            <a href="{{route('member.show', $item->id) }}">
                                                <button type="button" class="btn btn-primary">Detail</button>
                                            </a>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection