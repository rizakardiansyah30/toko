@extends('layouts.template')

@section('content')
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h2>Tambah Kategori Anggota</h2>
        </div>
        <form action="{{ route('member_category.store') }}" method="post">
            @csrf
        <div class="body">
            <form id="form_validation" method="POST" novalidate="novalidate">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input name="name" cols="30" rows="1" class="form-control no-resize" required="" aria-required="true"></input>
                        <label class="form-label">Name</label>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <input name="discount" cols="30" rows="1" class="form-control no-resize" required="" aria-required="true"></input>
                        <label class="form-label">Diskon</label>
                    </div>
                </div>
                <button class="btn btn-danger waves-effect" type="submit">Simpan</button>
            </form>
        </div>
    </div>
</div>
@endsection