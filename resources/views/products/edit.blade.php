@extends('layouts.template')

@section('content')
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h2>Tambah Kategori Barang</h2>
        </div>
        <form action="{{ route('product.update', $product ->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('put')
        <div class="body">
            <form id="form_validation" method="POST" novalidate="novalidate">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="name" required="" aria-required="true" value="{{ $product->name }}">
                        <label class="form-label">Name</label>
                    </div>
                </div>
                <div class="form-group form-float">
                <div>
                        <label">Gambar</label>
                        <input type="file" class="form-control" name="image" required="" aria-required="true">
                        <div class="form-control show-tick"></div>
                    </div>
                </div>
                <div class="form-group form-float">
                        <label">Produk Kategori</label>
                        <select class="form-control show-tick" name="category" id="category">
                            <option>chose...</option>
                                @foreach ($category as $item)
                                <option value="{{ $item->id }}" {{ $product->product_category_id == $item->id ? 'selected' : ''}}> {{ $item->name }}</option>
                                @endforeach
                        </select>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <input name="desc" cols="30" rows="2" class="form-control no-resize" required="" aria-required="true" value="{{ $product->desc }}"></input>
                        <label class="form-label">Description</label>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <input name="price" cols="30" rows="1" class="form-control no-resize" required="" aria-required="true" value="{{ $product->price }}"></input>
                        <label class="form-label">Harga</label>
                    </div>
                </div>
                <button class="btn btn-primary waves-effect" type="submit">SIMPAN</button>
            </form>
        </div>
    </div>
</div>
@endsection
