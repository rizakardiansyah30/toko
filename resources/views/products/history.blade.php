@extends('layouts.template')

@section('content')
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h2>
                Histori Perbelanjaan
            </h2>
        </div>
        <div class="body">
            <table id="mainTable" class="table table-striped" style="cursor: pointer;">
                <thead>
                <tr>
                    <th>NO</th>
                    <th>KODE TRANSAKSI</th>
                    <th>NAMA LENGKAP</th>
                    <th>JUMLAH</th>
                    <th>DISKON</th>
                    <th>TOTAL</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($products->pinjam as $i => $item)
                        <tr>
                            <th>{{ $i+1 }}</th>
                            <td>{{ $item->trx_number }}</td>
                            <td>{{ $item->categRef->full_name }}</td>
                            <td>{{ $item->quantity }}</td>
                            <td>Rp.{{ $item->discount }}</td>
                            <td>Rp.{{ $item->total}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        <input style="position: absolute; display: none;"></div>
    </div>
</div>
@endsection
