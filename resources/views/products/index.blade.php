@extends('layouts.template')

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Data Barang
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li>
                        <a href="{{ route('product.create') }}" >
                            <button type="button" class="btn btn-danger btn-sm">Tambah</button>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body table-responsive">
                <table class="table table-condensed">
                    <thead>
                        <tr class="bg-deep-orange">
                            <th>NO</th>
                            <th>NAMA BARANG</th>
                            <th>GAMBAR</th>
                            <th>PRODUK KATEGORI</th>
                            <th>DESKRIPSI</th>
                            <th>HARGA</th>
                            <th>PILIHAN</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($products as $i => $item)
                        <tr class="bg-danger">
                            <th>{{ $i+1 }}</th>
                            <td>{{ $item->name }}</td>
                            <td> <img src="{{ URL::to('/') }}/uploads/products/{{$item->image}}" class="img-thumbnail" width="100px" height="100px" /></td>
                            <td>{{ $item->categoryRef->name }}</td>
                            <td>{{ $item->desc }}</td>
                            <td> Rp.{{ $item->price }}</td>
                            <td>
                                <form action="{{ route ('product.destroy',$item->id) }}" method="post">
                                    <a href="{{ route ('product.edit', $item->id) }}">
                                        <button type="button" class="btn btn-success">Ubah</button>
                                    </a>
                                    @csrf 
                                    @method('delete')
                                        <button type="submit" class="btn btn-danger" 
                                        onclick="return confirm('Yakin Dihapus?' )">Hapus</button>
                                        <a href="{{route('product.show', $item->id) }}">
                                            <button type="button" class="btn btn-primary">Detail</button>
                                        </a>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection