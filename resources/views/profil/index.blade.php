@extends('layouts.template')

@section('content')
<div class="col-xs-12 col-sm-12">
    <div class="card profile-card">
        <div class="profile-header">&nbsp;</div>
        <div class="profile-body">
            <div class="image-area">
                <img src="{{ asset ('rzk.jpg') }}" style="height: 160px; width: 140px;" alt="AdminBSB - Profile Image">
            </div>
            <div class="content-area">
                <P> RIZAK ARDIANSYAH </P>
                <P> JEPARA, 03-07-2002 </P>
                <P> CEPOGO WATUROYO RT 04/12 </P>
                <P> SMK NEGERI 1 BANGSRI </P>
            </div>
        </div>
        <div class="profile-footer">
            <ul>
                <li>
                    <span>Facebook</span>
                    <span>Rizak Ardi S</span>
                </li>
                <li>
                    <span>Instagram</span>
                    <span>rizakardi_027</span>
                </li>
                <li>
                    <span>Whatsapp</span>
                    <span>089635703488</span>
                </li>
                <li>
                    <span>Email</span>
                    <span>rizakardiansyah30@gmail.com</span>
                </li>
            </ul>
            <button class="btn btn-primary btn-lg waves-effect btn-block">FOLLOW</button>
        </div>
    </div>
</div>
@endsection