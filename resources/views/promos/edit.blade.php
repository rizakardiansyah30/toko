@extends('layouts.template')

@section('content')
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h2>Edit Data Promosi</h2>
        </div>
        <form action="{{ route('promos.update', $promo->id) }}" method="post">
            @csrf
            @method('put')
        <div class="body">
            <form id="form_validation" method="POST" novalidate="novalidate">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="name" required="" aria-required="true" value=" {{ $promo->name }}">
                        <label class="form-label">Name</label>
                    </div>
                </div>
                <div class="form-group form-float">
                        <label">Produk Kategori</label>
                        <select class="form-control show-tick" name="category" id="category">
                            <option>chose...</option>
                                @foreach ($category as $item)
                                <option value="{{ $item->id }}" {{ $promo->product_id == $item->id ? 'selected' : ''}}> {{ $item->name }}</option>
                                @endforeach
                        </select>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <input name="discount" cols="30" rows="1" class="form-control no-resize" required="" aria-required="true" value=" {{ $promo->discount }}"></input>
                        <label class="form-label">Diskon</label>
                    </div>
                </div>
                <button class="btn btn-danger waves-effect" type="submit">SIMPAN</button>
            </form>
        </div>
    </div>
</div>
@endsection
