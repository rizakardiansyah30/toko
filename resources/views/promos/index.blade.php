@extends('layouts.template')

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Promo
                </h2>
                <ul class="header-dropdown m-r--5">
                <li>
                    <a href="{{ route('promos.create') }}" >
                        <button type="button" class="btn btn-danger btn-sm">Tambah</button>
                    </a>
                 </li>
                </ul>
            </div>
            <div class="body table-responsive">
                <table class="table table-condensed">
                    <thead>
                        <tr class="bg-deep-orange">
                            <th>NO</th>
                            <th>NAMA</th>
                            <th>KATEGORI</th>
                            <th>DISKON</th>
                            <th>PILIHAN</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($promos as $i => $item)
                        <tr class="bg-danger">
                            <th>{{ $i+1 }}</th>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->categoryRef->name }}</td>
                            <td>{{ $item->discount }}</td>
                            <td>
                                <form action="{{ route ('promos.destroy',$item->id) }}" method="post">
                                    <a href="{{ route ('promos.edit', $item->id) }}">
                                        <button type="button" class="btn btn-success">Ubah</button>
                                    </a>
                                    @csrf 
                                    @method('delete')
                                        <button type="submit" class="btn btn-danger" 
                                        onclick="return confirm('Yakin Dihapus?' )">Hapus</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection