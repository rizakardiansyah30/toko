@extends('layouts.template')

@section('content')
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h2>Edit Data Transaksi</h2>
        </div>
        <form action="{{ route('transaksi.update', $transaksi->id) }}" method="post">
            @csrf
            @method('put')
        <div class="body">
            <form id="form_validation" method="POST" novalidate="novalidate">
                <div class="form-group form-float">
                        <label">Nama Produk</label>
                        <select class="form-control show-tick" name="products" id="products">
                            <option>chose...</option>
                                @foreach ($product as $item)
                                <option value="{{ $item->id }}" {{ $transaksi->product_id == $item->id ? 'selected' : ''}}> {{ $item->name }}</option>
                                @endforeach
                        </select>
                </div>
                <div class="form-group form-float">
                        <label">Nama Lengkap</label>
                        <select class="form-control show-tick" name="member" id="member">
                            <option>chose...</option>
                                @foreach ($member as $item)
                                <option value="{{ $item->id }}" {{ $transaksi->member_id == $item->id ? 'selected' : ''}}> {{ $item->full_name }}</option>
                                @endforeach
                        </select>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <input name="quantity" cols="30" rows="1" class="form-control no-resize" required="" aria-required="true" value=" {{ $transaksi->quantity }}"></input>
                        <label class="form-label">Jumlah</label>
                    </div>
                </div>
                <button class="btn btn-danger waves-effect" type="submit">SIMPAN</button>
            </form>
        </div>
    </div>
</div>
@endsection
