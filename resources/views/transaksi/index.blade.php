@extends('layouts.template')

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Transaksi
                </h2>
                <ul class="header-dropdown m-r--5">
                <li>
                    <a href="{{ route('transaksi.create') }}" >
                        <button type="button" class="btn btn-danger btn-sm">Tambah</button>
                    </a>
                 </li>
                </ul>
            </div>
            <div class="body table-responsive">
                <table class="table table-condensed">
                    <thead>
                        <tr class="bg-deep-orange">
                            <th>NO</th>
                            <th>KODE TRANSAKSI</th>
                            <th>NAMA PRODUK</th>
                            <th>NAMA LENGKAP</th>
                            <th>JUMLAH</th>
                            <th>DISKON</th>
                            <th>TOTAL</th>
                            <th>PILIHAN</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($transaksis as $i => $item)
                        <tr class="bg-danger">
                            <th>{{ $i+1 }}</th>
                            <td>{{ $item->trx_number }}</td>
                            <td>{{ $item->categoryRef->name }}</td>
                            <td>{{ $item->categRef->full_name }}</td>
                            <td>{{ $item->quantity }}</td>
                            <td>Rp.{{ $item->discount }}</td>
                            <td>Rp.{{ $item->total}}</td>
                            <td>
                                <form action="{{ route ('transaksi.destroy',$item->id) }}" method="post">
                                    <a href="{{ route ('transaksi.edit', $item->id) }}">
                                        <button type="button" class="btn btn-success">Ubah</button>
                                    </a>
                                    @csrf 
                                    @method('delete')
                                        <button type="submit" class="btn btn-danger" 
                                        onclick="return confirm('Yakin Dihapus?' )">Hapus</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
